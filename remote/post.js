import fetch from 'isomorphic-unfetch';

const api = async (url, opts = {}) => {
  const req = await fetch(`http://127.0.0.1:8000/api/${url}`, opts);
  const res = await req.json();

  return res.data;
};

const getAllPosts = async () => api('posts');
const getPost = async id => api(`posts/${id}`);
const upVotePost = async id => api(`posts/${id}/votes/up`, { method: 'PATCH' });
const downVotePost = async id => api(`posts/${id}/votes/down`, { method: 'PATCH' });

export {
  getAllPosts,
  getPost,
  upVotePost,
  downVotePost,
};
