module.exports = {
  setupFiles: ['<rootDir>/jest.setup.js'],
  testPathIgnorePatterns: ['<rootDir>/.next/', '<rootDir>/node_modules/'],
  testEnvironment: 'node',
  moduleNameMapper: {
    '^@comp(.*)': '<rootDir>/components/$1',
    '^@lib(.*)': '<rootDir>/library/$1',
    '^@remote(.*)': '<rootDir>/remote/$1',
    '^@pages(.*)': '<rootDir>/pages/$1',
  },
};
