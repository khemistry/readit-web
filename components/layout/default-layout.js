import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Header from '@comp/static/header';

const Block = styled.div`
  width: 100%;
  max-width: 1440px;
  margin: 0 auto;
`;

export default function DefaultLayout({ children }) {
  return (
    <Block>
      <Header />
      {children}
    </Block>
  );
}

DefaultLayout.propTypes = {
  children: PropTypes.element.isRequired,
};
