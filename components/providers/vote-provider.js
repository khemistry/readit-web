import { Component } from 'react';
import PropTypes from 'prop-types';
import { upVotePost, downVotePost } from '@remote/post';

export default class VoteProvider extends Component {
  constructor(props) {
    super(props);

    const { post } = props;
    this.state = {
      votes: post.votes,
    };
  }

  updateVotes = (postData) => {
    this.setState({
      votes: postData.votes,
    });
  }

  upVote = () => {
    const { post } = this.props;
    upVotePost(post.id).then(this.updateVotes);
  }

  downVote = () => {
    const { post } = this.props;
    downVotePost(post.id).then(this.updateVotes);
  }

  render() {
    const { votes } = this.state;
    const { children } = this.props;

    return children({
      votes,
      upVote: this.upVote,
      downVote: this.downVote,
    });
  }
}

VoteProvider.propTypes = {
  children: PropTypes.func.isRequired,
  post: PropTypes.shape({
    id: PropTypes.number.isRequired,
    votes: PropTypes.number.isRequired,
    link: PropTypes.string.isRequired,
    details: PropTypes.string.isRequired,
  }).isRequired,
};
