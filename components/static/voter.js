import React, { Component } from 'react';
import styled, { css } from 'styled-components';
import PropTypes from 'prop-types';

const voteTypes = {
  neutral: '__NEUTRAL__',
  up: '__UP__',
  down: '__DOWN__',
};

const iconMap = {
  up: '▲',
  down: '▼',
};

const Block = styled.div`
  align-items: center;
  display: inline-flex;
  margin: 0 auto;
`;

const Button = styled.button`
  background: transparent;
  border: 0;
  cursor: pointer;
  font-size: 24px;
  opacity: 0.5;
  outline: 0;
  padding: 8px;
  transform: scale3d(1, 1, 1);
  transition: transform 200ms ease-out, opacity 200ms ease-out;

  ${props => props.voteType === voteTypes.neutral && css`
    &:hover,
    &:focus {
      opacity: 1;
      transform: scale3d(1.2, 1.2, 1.2);
    }
  `}

  ${props => props.variant === 'up' && css`
    color: green;
  `}

  ${props => props.variant === 'down' && css`
    color: red;
  `}

  ${props => props.voteType !== voteTypes.neutral && css`
    opacity: 0.1;
    color: black;
  `}
`;

const Votes = styled.div`
  font-size: 34px;

  ${props => props.voteType === voteTypes.up && css`
    color: green;
  `}

  ${props => props.voteType === voteTypes.down && css`
    color: red;
  `}
`;

export default class Voter extends Component {
  constructor(props) {
    super(props);

    this.state = {
      voteType: voteTypes.neutral,
    };
  }

  doUpVote = upVote => () => {
    this.setState({
      voteType: voteTypes.up,
    });

    upVote();
  };

  doDownVote = downVote => () => {
    this.setState({
      voteType: voteTypes.down,
    });

    downVote();
  };

  hasUserVoted = () => {
    const { voteType } = this.state;
    return voteType !== voteTypes.neutral;
  }

  renderButton = ({ variant, onClickFn }) => {
    const { voteType } = this.state;
    const isDisabled = this.hasUserVoted();

    const props = {
      variant,
      voteType,
      onClick: onClickFn,
      disabled: isDisabled,
    };

    return <Button {...props}>{iconMap[variant]}</Button>;
  }

  render() {
    const { votes, upVote, downVote } = this.props;
    const { voteType } = this.state;

    return (
      <Block>
        {this.renderButton({ variant: 'up', onClickFn: this.doUpVote(upVote) })}
        <Votes voteType={voteType}>{votes}</Votes>
        {this.renderButton({ variant: 'down', onClickFn: this.doDownVote(downVote) })}
      </Block>
    );
  }
}

Voter.propTypes = {
  votes: PropTypes.number.isRequired,
  upVote: PropTypes.func.isRequired,
  downVote: PropTypes.func.isRequired,
};
