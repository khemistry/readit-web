import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const Block = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
`;

export default function PostGrid({ children }) {
  return (
    <Block>
      {children}
    </Block>
  );
}

PostGrid.propTypes = {
  children: PropTypes.arrayOf(
    PropTypes.element,
  ).isRequired,
};
