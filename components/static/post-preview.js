import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { summary, urlBase } from '@lib/string/utils';
import { getBackground } from '@lib/post/utils';

const Block = styled.div`
  color: black;
  padding: 20px;
  text-align: center;
  margin-top: 16px;
`;

const Votes = styled.div`
  background: ${getBackground};
  color: white;
  display: inline-block;
  font-size: 34px;
  min-width: 100px;
  text-align: center;
  transform: skew(-10deg) rotateZ(-10deg);
`;

const Link = styled.div`
  font-size: 28px;
  margin-bottom: 4px;
  margin-top: 32px;
`;

const Summary = styled.div`
  color: rgb(90, 90, 90);
`;

export default function PostPreview({ votes, link, details }) {
  return (
    <Block>
      <Votes votes={votes}>{votes}</Votes>
      <Link>{urlBase(link)}</Link>
      <Summary>{summary(details)}</Summary>
    </Block>
  );
}

PostPreview.propTypes = {
  votes: PropTypes.number.isRequired,
  link: PropTypes.string.isRequired,
  details: PropTypes.string.isRequired,
};
