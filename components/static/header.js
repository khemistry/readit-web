import React from 'react';
import styled from 'styled-components';
import Link from 'next/link';

const Block = styled.div`
  text-align: center;
  margin-bottom: 32px;
`;

const Logo = styled.div`
  border-bottom: 2px solid #424242;
  font-family: 'Lobster', cursive;
  font-size: 48px;
  margin: 0 auto;
  max-width: 200px;
  width: 100%;
`;

const Nav = styled.div`
  margin-top: 8px;
`;

export default function Header() {
  return (
    <Block>
      <Logo>Readit</Logo>
      <Nav>
        <Link prefetch href="/">
          <a>View posts</a>
        </Link>
      </Nav>
    </Block>
  );
}
