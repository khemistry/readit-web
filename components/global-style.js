import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  @import url('https://fonts.googleapis.com/css?family=Lato|Lobster');

  body {
    background-color: #fafafa;
    color: #212121;
    font-family: 'Lato', sans-serif;
  }

  a {
    text-decoration: none;
  }
`;

export default GlobalStyle;
