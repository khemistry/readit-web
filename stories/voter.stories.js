import React, { Fragment } from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import Voter from '@comp/static/voter';
import GlobalStyle from '@comp/global-style';
import '@lib/mocks/router';

storiesOf('Voter', module)
  .addDecorator(storyFn => (
    <Fragment>
      <GlobalStyle />
      {storyFn()}
    </Fragment>
  ))
  .add('vanilla', () => (
    <Voter
      votes={234}
      upVote={action('upVote')}
      downVote={action('downVote')}
    />));
