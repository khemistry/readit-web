import React, { Fragment } from 'react';
import { storiesOf } from '@storybook/react';
import PostPreview from '@comp/static/post-preview';
import GlobalStyle from '@comp/global-style';

const link = 'https://example.com';
const details = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';

storiesOf('PostPreview', module)
  .addDecorator(storyFn => (
    <Fragment>
      <GlobalStyle />
      {storyFn()}
    </Fragment>
  ))
  .add('vanilla', () => (
    <PostPreview
      link={link}
      details={details}
      votes={0}
    />))
  .add('high votes', () => (
    <PostPreview
      link={link}
      details={details}
      votes={200}
    />))
  .add('negative votes', () => (
    <PostPreview
      link={link}
      details={details}
      votes={-150}
    />));
