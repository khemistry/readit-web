import React, { Fragment } from 'react';
import { storiesOf } from '@storybook/react';
import Header from '@comp/static/header';
import GlobalStyle from '@comp/global-style';
import '@lib/mocks/router';

storiesOf('Header', module)
  .addDecorator(storyFn => (
    <Fragment>
      <GlobalStyle />
      {storyFn()}
    </Fragment>
  ))
  .add('vanilla', () => (
    <Header />));
