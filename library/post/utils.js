const getGreen = val => `rgb(0, ${val}, 0)`;

const getRed = val => `rgb(${Math.abs(val) * 5}, 0, 0)`;

const getBackground = ({ votes }) => (
  votes >= 0
    ? getGreen(votes)
    : getRed(votes)
);

export {
  getGreen,
  getRed,
  getBackground,
};
