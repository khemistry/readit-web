import {
  invoker, pipe, split, take, join, drop,
} from 'ramda';

export const endsWith = invoker(1, 'concat');

export const summary = pipe(
  split(' '),
  take(10),
  join(' '),
  endsWith('...'),
);

export const urlBase = pipe(
  split('/'),
  drop(2),
  take(1),
  join(''),
);
