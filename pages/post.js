import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { getPost } from '@remote/post';
import VoteProvider from '@comp/providers/vote-provider';
import Voter from '@comp/static/voter';

const Center = styled.div`
  text-align: center;
`;

const renderVoter = ({ votes, upVote, downVote }) => (
  <Voter
    votes={votes}
    upVote={upVote}
    downVote={downVote}
  />
);

export default function Post({ post }) {
  return (
    <Center>
      <VoteProvider post={post}>
        {renderVoter}
      </VoteProvider>
      <h2>
        <a href={post.link}>{post.link}</a>
      </h2>
      <p>{post.details}</p>
    </Center>
  );
}

Post.getInitialProps = async ({ query }) => ({
  post: await getPost(query.id),
});

Post.propTypes = {
  post: PropTypes.shape({
    id: PropTypes.number.isRequired,
    votes: PropTypes.number.isRequired,
    link: PropTypes.string.isRequired,
    details: PropTypes.string.isRequired,
  }).isRequired,
};

renderVoter.propTypes = {
  votes: PropTypes.number.isRequired,
  upVote: PropTypes.func.isRequired,
  downVote: PropTypes.func.isRequired,
};
