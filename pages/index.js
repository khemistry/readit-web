import React, { Fragment } from 'react';
import * as R from 'ramda';
import PropTypes from 'prop-types';
import Link from 'next/link';
import { getAllPosts } from '@remote/post';
import PostPreview from '@comp/static/post-preview';
import PostGrid from '@comp/static/post-grid';

const renderPostPreview = post => (
  <PostPreview
    link={post.link}
    details={post.details}
    votes={post.votes}
  />
);

const renderPosts = R.map(post => (
  <Link
    key={post.id}
    prefetch
    href={{ pathname: '/post', query: { id: post.id } }}
  >
    <a>
      {renderPostPreview(post)}
    </a>
  </Link>
));

export default function Posts({ posts }) {
  return (
    <Fragment>
      <PostGrid>
        {renderPosts(posts)}
      </PostGrid>
    </Fragment>
  );
}

Posts.getInitialProps = async () => ({
  posts: await getAllPosts(),
});

Posts.propTypes = {
  posts: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      votes: PropTypes.number.isRequired,
      link: PropTypes.string.isRequired,
      details: PropTypes.string.isRequired,
    }),
  ),
};

Posts.defaultProps = {
  posts: [],
};
