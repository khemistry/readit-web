const path = require('path');

function resolve(dir) {
  return path.join(__dirname, dir);
}

module.exports = {
  webpack: config => Object.assign({}, config, {
    resolve: {
      alias: {
        '@comp': resolve('components'),
        '@lib': resolve('library'),
        '@remote': resolve('remote'),
        '@pages': resolve('pages'),
      },
    },
  }),
};
